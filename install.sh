#!/bin/sh

echo "This script will install dwSampleFramework for Ubuntu Linux."

# Dependency for Ubuntu
sudo apt install libx11-dev libxrandr-dev libxinerama-dev libxcursor-dev libxi-dev mesa-common-dev libglu1-mesa-dev

# Getting dwSampleFramework
git clone https://github.com/diharaw/dwSampleFramework.git &&
echo "dwSampleFramework clone done." &&
cd dwSampleFramework &&

# Create lib directory if it doesn't exist.
EXT_DIR="./external";
if [ ! -d "$EXT_DIR" ]; then
    echo "Creating external directory";
	mkdir external;
fi
cd ./external &&\

# Getting GLFW
git clone https://github.com/glfw/glfw.git &&
echo "GLFW install done."

# Getting assimp
git clone https://github.com/assimp/assimp.git &&
echo "assimp install done."

# Getting glm
git clone https://github.com/g-truc/glm.git &&
echo "glm install done."

# Getting imgui
git clone https://github.com/ocornut/imgui.git &&
echo "imgui install done."

# Getting stb
git clone https://github.com/nothings/stb.git &&
echo "stb install done."

# Getting json
git clone https://github.com/nlohmann/json.git &&
echo "json install done."

# Getting vk
git clone https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator.git &&
echo "vk install done."

